﻿
    P_Coords(i_loc,i_lonlat) := data table 
                        lon          lat
             !  -----------  -----------
Berkel-Enschot   5.13146886  51.58018575
       Haarlem   4.64355970  52.38370575
       Hamburg  10.00065400  57.55034100
     Amsterdam   4.89797551  52.37454030
         Paris   2.35149920  48.85661010
    Copenhagen  12.57007240  55.68672430
        Berlin  13.38885990  52.51703650
      Brussels   4.35169700  50.84655650
    ;


